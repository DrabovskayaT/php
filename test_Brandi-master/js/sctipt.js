var timerSubmit;

var menu_selector = ".menu"; // Переменная должна содержать название класса или идентификатора, обертки нашего меню.
function onScroll() {
    var scroll_top = $(document).scrollTop();
    $(menu_selector + " a").each(function () {
        var hash = $(this).attr("href");
        var target = $(hash);
        if (target.position().top <= scroll_top + 200 && target.position().top + target.outerHeight() > scroll_top) {
            $(menu_selector + " a.active").removeClass("active");
            $(this).addClass("active");
        } else {
            $(this).removeClass("active");
        }
    });
}
$(document).ready(function () {
    $(document).on("scroll", onScroll);
    $("a[href^=#]").click(function (e) {
        e.preventDefault();
        $(document).off("scroll");
        $(menu_selector + " a.active").removeClass("active");
        $(this).addClass("active");
        var hash = $(this).attr("href");
        var target = $(hash);
        $("html, body").animate({
            scrollTop: target.offset().top
        }, 500, function () {
            window.location.hash = hash;
            $(document).on("scroll", onScroll);
        });
    });
});

jQuery.validator.addMethod("accept", function (value, element, param) {
    return value.match(new RegExp("." + param + "$"));
});

$(function () {
    $('#phone').mask("8(999)999-9999");
    $.validator.addMethod("alphabetsnspace", function (value, element) {
        return this.optional(element) || /^[a-zA-ZА-Яа-я ]*$/.test(value);
    });
    $('#form').validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            name: {
                required: true,
                minlength: 2,
                alphabetsnspace: true
            },
            phone: {
                required: true,
            },
            city: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Поле обязательно к заполнению",
                minlength: "Имя не может быть меньше 2-х символов",
                alphabetsnspace: "Поле должно содержать только буквы"
            },
            email: {
                required: "Поле обязательно к заполнению",
                email: "Введите корректный email"
            },
            phone: {
                required: "Поле обязательно к заполнению"
            },
            city: {
                required: "Поле обязательно к заполнению"
            }
        },
        submitHandler: showModal
    });
});


function showModal() {
    $('#modal').show();
    timerSubmit = setTimeout(closeModal, 4000);
}

$('.modul__close').click(function () {
    clearTimeout(timerSubmit);
    closeModal();
})

function closeModal() {
    $('#modal').hide();
}

$(window).on('load resize',function() {
    if ($(window).width() <= '768'){
        $('.bar').show();
        $('.navigation').hide();
    } else {
        $('.bar').hide();
        $('.navigation').show();
    }
})

$('.bar').click(()=>{
    $('.navigation').slideToggle('slow');
})
